import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Footer, FooterTab, Icon, Text,View } from 'native-base';
import BottomNavigation, {
    FullTab
} from 'react-native-material-bottom-navigation';


const tabs = [
    {
      key: '1',
      icon: 'home',
      label: 'Home',
      barColor: '#f36523',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: '2',
      icon: 'eye',
      label: 'Schedule',
      barColor: '#f36523',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: '3',
      icon: 'person',
      label: '',
      barColor: 'powderblue',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
        key: '4',
        icon: 'checkmark',
        label: 'Attendance',
        barColor: '#f36523',
        pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
        key: '5',
        icon: 'clipboard',
        label: 'Mark',
        barColor: '#f36523',
        pressColor: 'rgba(255, 255, 255, 0.16)'
    }
]

export default class TabFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            target: this.props.target,
        }
    }

    handleTouchFooter(target){
        this.props.actionPageFooter(target);
        this.setState({
            target,
        })
    }

    componentWillReceiveProps(nextProps) {
       if (this.state.target !== nextProps.target){
           this.setState({
               target: nextProps.target,
           })
       }
    }

    renderIcon = icon => ({ isActive }) => (
        <Icon size={16} color="white" name={icon} />
      )
     
    renderTab = ({ tab, isActive }) => (
        <FullTab
            isActive={isActive}
            key={tab.key}
            label={tab.label}
            renderIcon={this.renderIcon(tab.icon)}
            labelStyle={[{color: 'black'},
                tab.key === this.props.target && { fontWeight: 'bold'},
            ]}
            style={[
                tab.key === '3' && styles.styleTabUser,
                styles.tabFull,
            ]}

        />
    )

    render() {
        const { target } = this.state;
        return (
            <BottomNavigation
                activeTab={target}
                onTabPress={newTab => this.props.actionPageFooter(newTab.key)}
                renderTab={this.renderTab}
                tabs={tabs}
            />
        );
    }
}

const styles = StyleSheet.create({
    styleTabUser: {
        backgroundColor: '#c2ece0', 
        borderRadius: 35, 
        bottom: 30, 
        position: 'relative', 
        height: 70,
        width: 70,
        minWidth: 70,
        paddingTop: 20,
    },
    tabFull: {
        minWidth: 70, 
    },
});



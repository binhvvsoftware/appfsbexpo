import React from 'react';
import { Container,} from 'native-base';
import { StyleSheet,Text,View, ActivityIndicator, WebView } from 'react-native';

export default class ItemDetail extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return  {
            headerTitle: (
                <View>
                  <Text style={{fontWeight: 'bold', fontSize: 16}}>{navigation.getParam('title')}</Text>
                </View>
              )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            fontLoaded: false,
        }
    }

    render () {
        const content = this.props.navigation.getParam('content');
        if (!content){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator  size="large" color="#f36523" />
                </View>
            );
        }
        return (
            <Container style={{marginTop: 20, flex: 1, alignItem: 'center', marginHorizontal: 20}}>
                <WebView 
                    source={{html: content.full_text}}
                    style={{ height: 300, width: '100%', flex: 1 }}
                    scrollEnabled={false}
                />
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});

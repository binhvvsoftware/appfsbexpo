import React from 'react';
import { StyleSheet, View, ImageBackground, Image, ActivityIndicator, AsyncStorage} from 'react-native';
import { Tab, Tabs, TabHeading,Text, Header } from 'native-base';
import background from '../assets/logoL.png';
import ProfileStudyItem from './ProfileStudyItem';
import ProfileActionItem from './ProfileActionItem';
import myHeader from './HeaderHome';
import { USER } from '../constants/hardcodeUser';

export default class Profile extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return  myHeader('Hồ sơ người dùng', navigation)
    }

    constructor(props) {
        super(props);
        this.state = {
            userInfo: [],
            isLoaded: false,
            myUser: {},
            infoMark: [],
        }
    }

    async componentDidMount(){
        const value = JSON.parse(await AsyncStorage.getItem('user'));
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const userLogin = JSON.parse(await AsyncStorage.getItem('userLogin'));

        if (value && (!Object.keys(this.state.myUser).length || value.user.email !== this.state.myUser.user.email)){
            this.setState({
                myUser: value,
            })
        }
        // this.setState({
        //     myUser: USER,
        // })
        this.getUserInfo(campusCode,userLogin);
        this.getInfoMark(campusCode,userLogin);
    }

    getUserInfo = async (campusCode,userLogin) => {
        const url = `http://ap.fsb.edu.vn/api/getStudentInfo.php?campus_code=${campusCode}&user_login=${userLogin}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                userInfo: responseJson.data,
                isLoaded: true,
            })
        }) 
    }

    getInfoMark = async(campusCode,userLogin) => {
        const url = `http://ap.fsb.edu.vn/api/getGradeHistory.php?campus_code=${campusCode}&user_login=${userLogin}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                infoMark: responseJson.result,
            })
        }) 
    }

    render () {
        const { myUser,userInfo,infoMark } = this.state;
        if (!Object.keys(myUser).length && !this.state.isLoaded){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator  size="large" color="#f36523" />
                </View>
            );
        }

        return (
            <View style={styles.container}>
                { this.props.navigation.getParam('isHeaderHome') && <Header hasTabs transparent /> }
                <View style={styles.headerSlide}>
                    <ImageBackground source={background} style={styles.myBackGround}>
                    </ImageBackground>
                    <View style={styles.headerInfo}>
                        <Image source={{uri: `${myUser.user.photoURL}`}} style={styles.myImage}></Image>
                        <Text style={styles.textName}>{myUser.user.displayName}</Text>
                        <Text>{myUser.user.email}</Text>
                    </View>
                </View>
                <View style={styles.contentSlide}>
                    <View style={{flex: 1,flexDirection: 'row', justifyContent: 'space-around',alignItems: 'center'}}>
                        <View style={[{alignItems: 'center',flex: 1},styles.borderText]}>
                            <Text style={styles.textName}>Điểm tích lũy</Text>
                            <Text style={styles.textName}>{infoMark.grade_avg}</Text>
                        </View>
                        <View style={{alignItems: 'center',flex: 1}}>
                            <Text style={styles.textName}>Tổng tín chỉ đã học</Text>
                            <Text style={styles.textName}>{infoMark.total_credit}</Text>
                        </View>
                    </View>
                    <View style={{flex: 6}}>
                        <ProfileStudyItem userInfo={userInfo} myUser={myUser}/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },  
    headerSlide: {
        flex: 1,
        backgroundColor: 'powderblue',
        paddingBottom: 10,
        paddingTop: 10,
    },
    contentSlide: {
        flex: 4,
    },
    myBackGround: {
        height: '100%',
        resizeMode: 'cover',
        opacity: 0.1,
        position: 'relative',
        bottom: 0,
        backgroundColor: 'powderblue',
    },
    headerInfo: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom : 20,
    },
    myImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
        marginBottom: 10,
    },
    contentIcon: {
        marginRight: 10,
    },
    textName: {
        fontWeight: 'bold',
        fontSize: 17, 
    },
    borderText: {
        borderRightWidth: 1,
        borderRightColor: '#b1b1b1',
    }
});

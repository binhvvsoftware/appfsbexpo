import React from 'react';
import { Container, Content, Thumbnail, Text, List, ListItem, Left, Body} from 'native-base';
import { StyleSheet, View } from 'react-native';
import HeaderSelectDay from './HeaderSelectDay';
import ScheduleItem from './ScheduleItem';

export default class Schedule extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            selected: 7
        };
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    onValueChange = (value) => {
        this.setState({
          selected: value
    })}

    render () {
        return (
            <Container>
                <HeaderSelectDay selected={this.state.selected} onValueChange={this.onValueChange} title="LỊCH HỌC"/>
                <ScheduleItem selectedDay={this.state.selected}/>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});

import React from 'react';
import { Container, Content, Thumbnail, Text, Left, Body, Card, CardItem, Icon,Header,Title,Right,Picker,Form} from 'native-base';

export default class ListNews extends React.Component {
    render () {
        return (
            <Header androidStatusBarColor='white' style={{ backgroundColor: 'white' }} noShadow>
                <Left style={{flex: 1}}>
                    <Title style={{color: 'black', fontWeight: 'bold', marginLeft: 10}}>{this.props.title}</Title>
                </Left>
                {
                    this.props.selected && (
                        <Content style={{flex: 1}}>
                            <Form style={{paddingLeft: 20}}>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Chọn Ngày"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={{ width: undefined }}
                                    selectedValue={this.props.selected}
                                    onValueChange={ (value) => this.props.onValueChange(value)}
                                >
                                    <Picker.Item label="7 ngày tới" value={7} />
                                    <Picker.Item label="14 ngày tới" value={14} />
                                    <Picker.Item label="30 ngày tới" value={30} />
                                    <Picker.Item label="60 ngày tới" value={60} />
                                    <Picker.Item label="90 ngày tới" value={90} />
                                    <Picker.Item label="7 ngày trước" value={-7} />
                                    <Picker.Item label="14 ngày trước" value={-14} />
                                    <Picker.Item label="30 ngày trước" value={-30} />
                                    <Picker.Item label="60 ngày trước" value={-60} />
                                    <Picker.Item label="90 ngày trước" value={-90} />
                                </Picker>
                            </Form>
                        </Content>
                    )
                }
                
            </Header>
        );
    }
}

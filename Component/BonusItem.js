import React from 'react';
import { Container, Content, Thumbnail, Text, Left, Body, Card, CardItem, Icon } from 'native-base';
import { StyleSheet, View, TouchableHighlight, FlatList, ActivityIndicator, RefreshControl} from 'react-native';

export default class ListContent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            page: 1,
            isLoading: false,
            refreshing: false,
        };
    }

    setModalVisible(item) {
        this.props.navigation.navigate('ListContentDetail',{ title: `Khen thưởng lần ${item.id}`, content: item});
    }
    
    componentDidMount(){
        this.getData();
    }

    getData = async () => {
        const url = `https://jsonplaceholder.typicode.com/albums?limit=30&_page=${this.state.page}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            if (responseJson.length === 0){
                this.setState({
                    isLoading: true,
                })
            }
            this.setState({
                dataSource: this.state.dataSource.concat(responseJson),
            })
        }) 
    }

    renderRow (item) {
        return (
            <Card>
                <TouchableHighlight  onPress={() => this.setModalVisible(item)}>
                    <CardItem>
                            <Left>
                                <Thumbnail square source={{ uri: 'https://znews-photo.zadn.vn/w660/Uploaded/wyhktpu/2019_06_19/5_3.jpg' }} />
                                <Content style={styles.contentCard}>
                                    <View >
                                        <Text style={styles.textTitle}>{item.id} - Khen thưởng thành tích học tập tốt năm học 2019-2020</Text>
                                    </View>
                                    <View style={styles.contentDetail}>
                                        <Icon name="calendar" style={styles.iconItem}/>
                                        <Text style={styles.textItem}>05/05/2020</Text>
                                    </View>
                                </Content>
                            </Left>
                    </CardItem>
                </TouchableHighlight>
            </Card>
        )
    }

    loadMoreData = () => {
       this.setState({
            page: this.state.page + 1,
       }, this.getData)
    }

    _onRefresh = async () => {
        await this.setState({
            refreshing: true,
            page: 1,
            dataSource: [],
        });
        await this.getData();
        this.setState({
            refreshing: false,
        })
    }

    renderFooter = () => {
        if (this.state.isLoading){
            return <Text style={{height: 0}}></Text>;
        }
       return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
            <ActivityIndicator size="large" color="#f36523"/>
        </View>
       )
    }

    render () {
        return (
            <Container>
                <FlatList
                    data={this.state.dataSource}
                    keyExtractor={(item,index) => index.toString()}
                    onEndReached={this.loadMoreData}
                    onEndReachedThreshold ={0.3}
                    renderItem={({item}) => this.renderRow(item)}
                    ListFooterComponent={ this.renderFooter}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />}
 
                />
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    contentCard: {
        marginLeft: 10,
    },
    contentDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    textItem: {
        color: '#cccccc',
        fontSize: 13,
    },
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: 1,
        marginRight: 7,
    },
    textTitle: {
        marginBottom: 5,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        minHeight: 50,
        fontSize: 14,
    }
});


import React from 'react';
import { List, ListItem, Text} from 'native-base';
import { StyleSheet, View, ActivityIndicator, AsyncStorage, FlatList, RefreshControl} from 'react-native';

export default class HistoryLessionItemDetail extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return  {
            title: 'Điểm chi tiết',
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            dataSource: [],
            isLoading: false,
        }
    }

    componentDidMount(){
        this.getData()
    }

    getData = async () => {
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const userLogin = JSON.parse(await AsyncStorage.getItem('userLogin'));
        const content = this.props.navigation.getParam('content');
        const url = `http://ap.fsb.edu.vn/api/getGradeDetailHistory.php?campus_code=${campusCode}&user_login=${userLogin}&groupid=${content.group_id}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                dataSource: responseJson,
                isLoading: true,
            })
        }) 
    }

    _onRefresh = async () => {
        await this.setState({
            dataSource: [],
            isLoading: false,
        });
        await this.getData();
    }

    renderRow = ({item}) => {
        return (
            <>
                <ListItem itemDivider>
                    <Text style={{fontWeight: 'bold'}}>Tên đầu điểm : {item.grade_name}</Text>
                </ListItem>                  
                <ListItem>
                    <Text>Trọng số : {item.grade_weight}</Text>
                </ListItem>
                <ListItem>
                    <Text>Điểm : {item.grade_value}</Text>
                </ListItem>
            </>
        )
    }

    render () {
        const { dataSource }  = this.state;

        if (!this.state.isLoading){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
                    <ActivityIndicator size="large" color="#f36523"/>
                </View>
            )
        }

        return (
            <View style={{flex: 1, marginHorizontal: 20 }}>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}><Text style={styles.textHeader}>{ dataSource.result.subject_name }</Text></View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text style={{fontWeight: 'bold'}}> Điểm trung bình : <Text style={{color: 'red'}}>{dataSource.result.grade_avg}</Text></Text>
                        <Text style={{fontWeight: 'bold'}}> Trạng thái : <Text style={dataSource.result.grade_status === 'Passed' ? { color: 'green'} : { color: '#008cba'}}>{dataSource.result.grade_status}</Text></Text>
                    </View>
                </View>
                <View style={{flex: 6, marginTop: 25 }}>
                    <List>
                        <FlatList
                            data={this.state.dataSource.data}
                            keyExtractor={(item,index) => index.toString()}
                            renderItem={(item) => this.renderRow(item)}
                            refreshControl={
                                <RefreshControl
                                    refreshing={false}
                                    onRefresh={this._onRefresh}
                                />}
                        />
                    </List>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textHeader: {
        fontWeight: 'bold',
        fontSize: 19,
    }
});

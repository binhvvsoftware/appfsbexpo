import React from 'react';
import { ScrollView } from 'react-native';
import {  List, ListItem ,Text, Content} from 'native-base';

export default class ProfileStudyItem extends React.Component {
    render () {
        const { userInfo, myUser } = this.props;
        return (
            <Content>
                <List style={{marginBottom: 20}}>
                    <ListItem>
                        <Text note>Tên đầy đủ : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.user_surname + ' ' + userInfo.user_middlename + ' ' + userInfo.user_givenname}</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Email : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.user_email}</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Mã sinh viên : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.user_code}</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Giới tính : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.gender}</Text>
                    </ListItem>
                    { userInfo.user_address && 
                        <ListItem>
                            <Text note>Địa chỉ : </Text>
                            <Text style={{fontWeight: 'bold'}}>{userInfo.user_address}</Text>
                        </ListItem>
                    }
                    <ListItem>
                        <Text note>Ngày sinh : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.user_DOB}</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Ngày nhập học : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.study_date_create}</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Trạng thái : </Text>
                        <Text style={{fontWeight: 'bold'}}>{userInfo.student_status}</Text>
                    </ListItem>
                </List>
            </Content>
        );
    }
}

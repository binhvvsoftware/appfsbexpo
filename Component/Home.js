import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image,ScrollView} from 'react-native';
import { ListItem } from 'native-base';
import iconAP from '../assets/mortarboard.png';
import iconLMS from '../assets/blackboard.png';
import iconEform from '../assets/books.png';
import iconOnline from '../assets/light-bulb.png';
import iconErule from '../assets/alarm-clock.png';
import iconEdoc from '../assets/open-book.png';
import iconEschedule from '../assets/test.png';
import iconEasd from '../assets/diploma.png';
import * as WebBrowser from 'expo-web-browser';

export default class Home extends React.Component {
    static navigationOptions = {
        title: 'My FSB',
    }

    constructor(props) {
        super(props);

    }

    handleWebview(url){
        WebBrowser.openBrowserAsync(url, {showTitle : true});
    }

    render () {
        const { navigation } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.listStudent}>
                    <ListItem itemDivider style={{marginBottom: 10}}>
                        <Text style={{fontWeight: 'bold',fontSize: 16}}>For Student</Text>
                    </ListItem>
                    <View style={{flex: 1}}>
                        <View style={styles.myItemStudent}>
                            <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('ScreenAP')} style={styles.contentItem}>
                                <Image source={iconAP} style={styles.myImage}/>
                                <Text style={{ alignSelf:'center' }}>AP</Text>
                            </TouchableOpacity>
                            <TouchableOpacity underlayColor="white" onPress={() => this.handleWebview('http://lms.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconLMS} style={styles.myImage}/>
                                <Text style={{ alignSelf:'center' }}>LMS</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.myItemStudent}>
                                <TouchableOpacity  onPress={() => this.handleWebview('http://eform.fsb.edu.vn/')} style={styles.contentItem}>
                                    <Image source={iconEform} style={styles.myImage}/>
                                    <Text style={{ alignSelf:'center' }}>EFORM</Text>
                                </TouchableOpacity>
                                <TouchableOpacity  onPress={() => this.handleWebview('http://online.fsb.edu.vn/')} style={styles.contentItem}>
                                    <Image source={iconOnline} style={styles.myImage}/>
                                    <Text style={{ alignSelf:'center' }}>ONLINE</Text>
                                </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.listStaff}>
                    <ListItem itemDivider style={{marginBottom: 10}}>
                        <Text style={{fontWeight: 'bold',fontSize: 16}}>For Faculty & Staff</Text>
                    </ListItem>
                    <View style={{flex: 1}}>
                        <View style={styles.myItemStaff}>
                            <TouchableOpacity onPress={() => navigation.navigate('ScreenAP')} style={styles.contentItem}>
                                <Image source={iconAP} style={styles.myImage}/>
                                <Text style={{alignSelf:'center'}}>AP</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.handleWebview('http://lms.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconLMS} style={styles.myImage}/>
                                <Text style={{alignSelf:'center'}}>LMS</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.myItemStaff}>
                            <TouchableOpacity onPress={() => this.handleWebview('http://eform.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconEform} style={styles.myImage}/>
                                <Text style={{alignSelf:'center'}}>EFORM</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.handleWebview('http://online.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconOnline} style={styles.myImage}/>
                                <Text style={{alignSelf:'center'}}>ONLINE</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.myItemStaff}>
                            <TouchableOpacity onPress={() => this.handleWebview('http://erule.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconErule} style={styles.myImage}/>
                                <Text style={{alignSelf:'center',justifyContent: 'center',alignItems: 'center'}}>ERULE</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.handleWebview('http://edoc.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconEdoc} style={styles.myImage}/>
                                <Text style={{alignSelf:'center'}}>EDOC</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.myItemStaff}>
                            <TouchableOpacity onPress={() => this.handleWebview('http://easd.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconEasd} style={styles.myImage}/>
                                <Text style={{alignSelf:'center'}}>EASD</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.handleWebview('http://eschedule.fsb.edu.vn/')} style={styles.contentItem}>
                                <Image source={iconEschedule} style={styles.myImageIcon}/>
                                <Text style={{alignSelf:'center'}}>ESCHEDULE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    listStudent: {
        flex: 1,
    },
    listStaff: {
        flex: 2,
    },
    myItemStudent: {
        height: '44%',
        minHeight: 65,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10,
        paddingHorizontal: 10,
    },
    myItemStaff: {
        height: '22%',
        minHeight: 65,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10,
        paddingHorizontal: 10,
    },
    contentItem: {
        width: '45%',
        justifyContent: 'center',
        alignItems: 'center', 
        borderRadius: 15,
        backgroundColor: 'powderblue',
    },
    myImage : {
        width: 45,
        height: 45,
    },
    myImageIcon: {
        width: 45,
        height: 45,
    }
});

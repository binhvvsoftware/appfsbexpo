import React from 'react';
import * as GoogleSignIn from 'expo-google-sign-in';
import { Container, Header } from 'native-base';
import { StyleSheet ,AsyncStorage} from 'react-native';
import TuitionFeeItem from './TuitionFeeItem';
import myHeader from './HeaderHome';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';

export default class TuitionFee extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return  myHeader('Học phí', navigation)
        
    }

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            selected: 0,
            isVisible: false,
            userInfo: {},
        };
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    componentDidMount(){
        this.getUserInfo();
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    getUserInfo = async () => {
        const url = 'https://jsonplaceholder.typicode.com/users/1';
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                userInfo: responseJson,
            })
        }) 
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    render () {
        return (
            <Container>
                <Header hasTabs transparent />
                <TuitionFeeItem userInfo={this.state.userInfo}/>
                <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});

import React from 'react';
import {  Thumbnail, Text, List, ListItem, Left, Body, Icon, Container} from 'native-base';
import { StyleSheet, View, FlatList, RefreshControl, AsyncStorage, TouchableHighlight, ActivityIndicator} from 'react-native';
import GradientButton from 'react-native-gradient-buttons';

export default class HistoryLessionItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            isLoading: false,
        };
    }

    setModalVisible(item) {
        this.props.navigation.navigate('HistoryLessionItemDetail',{ content: item});
    }

    componentDidMount(){
        this.getData();
    }

    getData = async () => {
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const userLogin = JSON.parse(await AsyncStorage.getItem('userLogin'));
        const url = `http://ap.fsb.edu.vn/api/getGradeHistory.php?campus_code=${campusCode}&user_login=${userLogin}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            this.setState({
                dataSource: responseJson,
                isLoading: true,
            })
        }) 
    }

    renderRow = ({item}) => {
        return (
                <ListItem thumbnail>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <GradientButton
                            text={item.grade}
                            gradientBegin="#ff631a"
                            gradientEnd="#f5a006"
                            width={70}
                            height={80}
                            radius={10}
                            impact
                            textStyle={{ color: 'black', fontSize: 15, fontWeight: 'bold' }}
                            onPressAction={() => this.setModalVisible(item)}
                        />

                    </View>
                    <TouchableHighlight onPress={() => this.setModalVisible(item)}  style={{flex: 4}}>
                        <Body style={{flex: 4}}>
                            <Text style={{fontSize: 17, fontWeight: 'bold'}}>{item.subject_name}</Text>
                            { item.status === 'Passed' && <Text style={{color: 'green', fontWeight: 'bold'}}>{item.status}</Text> }
                            { item.status === 'Learning' && <Text style={{color: '#008cba',fontWeight: 'bold'}}>{item.status}</Text>}
                            <View style={{flexDirection: 'row', marginTop: 5}}>
                                <Icon name="school" style={styles.iconItem}/>
                                <Text note style={styles.textItem}>{item.group_name}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Icon name="contact" style={styles.iconItem}/>
                                <Text note style={styles.textItem}> Tín chỉ: {item.number_of_credit}</Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <Icon name="laptop" style={styles.iconItem}/>
                                <Text note style={styles.textItem}>{item.subject_code}</Text>
                            </View>
                        </Body>
                    </TouchableHighlight>
                </ListItem>
        )
    }

    _onRefresh = async () => {
        await this.setState({
            dataSource: [],
            isLoading: false,
        });
        await this.getData();
    }

    render () {
        if (!this.state.isLoading){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
                    <ActivityIndicator size="large" color="#f36523"/>
                </View>
            )
        }
        return (
            <Container>
                <View style={styles.containerHeader}>
                    <Text style={styles.textHeader}>Tổng tín chỉ đã qua môn: {this.state.dataSource.result && this.state.dataSource.result.total_credit}</Text>
                    <Text style={styles.textHeader}>Điểm trung bình : {this.state.dataSource.result && this.state.dataSource.result.grade_avg}</Text>
                </View>
                <List style={styles.mainContent}>
                    <FlatList
                        data={this.state.dataSource.data}
                        keyExtractor={(item,index) => index.toString()}
                        renderItem={(item) => this.renderRow(item)}
                        refreshControl={
                            <RefreshControl
                                refreshing={false}
                                onRefresh={this._onRefresh}
                            />}
                    />
                </List>
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: 2,
        marginRight: 10,
    },
    containerHeader: {
        flex: 1,
        alignItems: 'center',
    },
    textHeader: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    mainContent: {
        flex: 10,
    }
});

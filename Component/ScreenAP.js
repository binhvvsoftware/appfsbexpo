import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground , AsyncStorage, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import { Content, Icon, Picker, Form, Body, Item, Label, Input, Toast } from "native-base";
import GradientButton from 'react-native-gradient-buttons';
import background from '../assets/education.jpeg';
import logoApp from '../assets/logoL.png';
import iconGoogle from '../assets/iconGoogle.png';
import * as GoogleSignIn from 'expo-google-sign-in';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';

export default class ScreenAP extends React.Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props);

        this.state = ({
            selected: 'fsbhn',
            user: null
        })
    }
    async onValueChange (value){
        this.setState({
            selected: value,
        });
    }

    componentDidMount() {
        this.checkTokenLogin();
    }

    checkTokenLogin = async() => {
        const value = JSON.parse(await AsyncStorage.getItem('user'));
        if (value.accessToken && value.accessToken.length !== 0){
            this.props.navigation.navigate('HomeAP');
            return
        }
    }

    // signInAsync = async () => {
    //     try {
    //         const value = JSON.parse(await AsyncStorage.getItem('user'));
    //         await AsyncStorage.setItem('campusCode', JSON.stringify(this.state.selected));
    //         if (value.accessToken && value.accessToken.length !== 0){
    //             this.props.navigation.navigate('HomeAP');
    //             return;
    //         }
    //         const result = await Google.logInAsync({
    //             ...GOOGLE_LOGIN,
    //             scopes: ['profile', 'email'],
    //         });
    //         if (result.type === 'success') {
    //             await AsyncStorage.setItem('user', JSON.stringify(result));
    //             // this.checkLoginUser(result);
    //             this.getUserInfo('son18mse13001')
    //         } else {
    //             console.log('cancel');
    //         }
    //     } catch (e) {
    //         console.log('cancel',e);
    //     }
    // }


    signInAsync = async () => {
        try {
            await GoogleSignIn.askForPlayServicesAsync();
            await AsyncStorage.setItem('campusCode', JSON.stringify(this.state.selected));
            const result = await GoogleSignIn.signInAsync();
            if (result.type === 'success') {
                    AsyncStorage.setItem('user', JSON.stringify(result))
                    this.checkLoginUser(result);
            }
        } catch ({ message }) {
            console.log(message);
            alert('login: Error:' + message);
        }
      };

    handleLogout = async() => {
        try {
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('userLogin', JSON.stringify(''));
            await AsyncStorage.setItem('user', JSON.stringify(''));
        } catch (e) {
            console.log('cancel',e);
        }
    }

    blockLoginUser = () => {
        this.handleLogout();
        alert('User của bạn không có quyền truy cập')
    }

    checkLoginUser = (info) => {
        if(info.user.email.indexOf('@fsb.edu.vn') === -1){
            this.blockLoginUser();
            return;
        }
        this.getUserInfo(info.user.email.replace('@fsb.edu.vn',''))
    }

    getUserInfo = async (userLogin) => {
        AsyncStorage.setItem('userLogin', JSON.stringify(userLogin))
        const url = `http://ap.fsb.edu.vn/api/getStudentInfo.php?campus_code=${this.state.selected}&user_login=${userLogin}`;
        fetch(url).then( response => response.json())
        .then(() => this.props.navigation.navigate('HomeAP'))
        .catch( err => this.blockLoginUser())
    }

    // hardcodeLogin = async() => {
    //     await AsyncStorage.setItem('campusCode', JSON.stringify(this.state.selected));
    //     this.getUserInfo('son19gem10048') 
    // }

    onChangeText = (text) => {
        this.setState({
            user: text,
        })
    }

    render () {
        return (
            <KeyboardAvoidingView
                style={{flex: 1}}
                keyboardVerticalOffset={0}
                behavior="padding"
                enabled
            >
                <ImageBackground source={background} style={styles.myBackGround}>
                    <View style={styles.wrapper}>
                        <Image source={logoApp} style={styles.myLogo}/>
                    </View>
                    <View style={styles.wrapperContent}>
                        <Text style={styles.textItem}>Chọn cơ sở</Text>
                        <Content>
                            <Form>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Chọn cơ sở"
                                    iosIcon={<Icon name="arrow-down" />}
                                    style={styles.myPicker}
                                    selectedValue={this.state.selected}
                                    onValueChange={this.onValueChange.bind(this)}
                                >
                                    <Picker.Item label="Hà Nội" value='fsbhn' />
                                    <Picker.Item label="Đà Nẵng" value='fsbdn' />
                                    <Picker.Item label="Hồ Chí Minh" value='fsbhcm' />
                                    <Picker.Item label="Tây Nguyên" value='fsbcth' />
                                    <Picker.Item label="Cần Thơ" value='minimba' />
                                </Picker>
                            </Form>

                            <Form style={{marginHorizontal: 10}}>
                                <Item floatingLabel>
                                    <Label>Username</Label>
                                    <Input />
                                </Item>
                                <Item floatingLabel>
                                    <Label>Password</Label>
                                    <Input />
                                </Item>
                            </Form>
                            <View style={styles.myButtonLogin}>
                                <GradientButton 
                                    text="Đăng nhập"
                                    gradientBegin="#ff631a"
                                    gradientEnd="#f5a006"
                                    gradientDirection="diagonal"
                                    height={50}
                                    width='77%'
                                    radius={25}
                                    impact
                                    impactStyle='Heavy'
                                    textStyle={{ color: 'black', fontSize: 18 }}
                                    // onPressAction={this.hardcodeLogin}
                                />
                            </View>
                            <TouchableOpacity  onPress={this.signInAsync}>
                                <Body style={styles.myButton}>
                                    <Image source={iconGoogle} style={styles.myLogoButton}/>
                                    <Text style={styles.textButton}>Sign in with Google</Text>
                                </Body>
                            </TouchableOpacity>
                        </Content>
                    </View>
                </ImageBackground>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    myBackGround: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
        flex: 1,
    },
    myLogo: {
        width: '100%',
        resizeMode: 'contain',
        marginTop: '0%',
    },
    wrapper: {
        flex: 1,
        justifyContent: 'center',
    },
    wrapperContent: {
        flex: 2,
        justifyContent: 'center',
    },
    textItem: {
        color: '#FF9800',
        fontSize: 22,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    textButton : {
        fontSize: 18,
        fontWeight: 'bold',
        marginRight: 20,
    },
    myPicker: {
        marginTop: 20,
        width: '50%',
        alignSelf: 'center',
        color: 'black',
    },
    myButton: {
        flexDirection: 'row', 
        justifyContent: 'space-around', 
        backgroundColor: 'white', 
        width: '77%', 
        height: 50, 
        borderRadius: 25,
        marginTop: 20,
    },
    myButtonLogin: {
        justifyContent: 'center', 
        alignItems: 'center',
        marginTop: '12%'
    },
    myLogoButton: {
        width: 35, 
        height: 35,
    }
});

import React from 'react';
import * as GoogleSignIn from 'expo-google-sign-in';
import { Container, Content, Thumbnail, Text, Left, Body, Card, CardItem, Icon, Header } from 'native-base';
import { StyleSheet, View, TouchableHighlight, ActivityIndicator, FlatList, RefreshControl, AsyncStorage} from 'react-native';
import GradientButton from 'react-native-gradient-buttons';
import HeaderSelectDay from './HeaderSelectDay';
import DialogLogout from './DialogLogout';

export default class Attendance extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return  myHeader('Attendance', navigation)
    }
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            isLoading: false,
            isVisible: false,
        };
    }

    componentDidMount(){
        this._isMounted = true;
        if (this.props.navigation.getParam('isHeaderHome')){
            this.props.navigation.setParams({
                onLogout: this.onPopupLogout.bind(this),
            })
        }
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    setModalVisible(item) {
        this.props.navigation.navigate('AttendanceDetail',{ content: item });
    }

    getData = async () => {
        const campusCode = JSON.parse(await AsyncStorage.getItem('campusCode'));
        const userLogin = JSON.parse(await AsyncStorage.getItem('userLogin'));
        const url = `http://ap.fsb.edu.vn/api/getAttendanceInfo.php?campus_code=${campusCode}&user_login=${userLogin}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            if (!this._isMounted){
                return;
            }
            this.setState({
                dataSource: responseJson,
                isLoading: true,
            })
        }) 
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    renderFooter = () => {
        if (this.state.isLoading){
            return <Text style={{height: 0}}></Text>;
        }
       return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
            <ActivityIndicator size="large" color="#f36523"/>
        </View>
       )
    }

    renderRow (item) {
        return (
        <Card>
            <TouchableHighlight onPress={() => this.setModalVisible(item)}>
                <CardItem>
                    <Left>
                        <GradientButton
                            text={item.group_name}
                            gradientBegin="#ff631a"
                            gradientEnd="#f5a006"
                            width={70}
                            height={80}
                            radius={10}
                            impact
                            textStyle={{ color: 'black', fontSize: 11 }}
                            onPressAction = {() => this.setModalVisible(item)}
                        />
                        <Content style={styles.contentCard}>
                            <View >
                                <Text style={styles.textTitle}>{item.subject_name}</Text>
                            </View>
                            <View style={styles.contentDetail}>
                                <Icon name="people" style={styles.iconItem}/>
                                <Text style={styles.textItem}>{`Vắng ${item.attendance_absent}/${item.total_to_now}`}</Text>
                                <Icon name="calendar" style={styles.iconItem}/>
                                <Text style={styles.textItem}>{`Tỉ lệ vắng ${item.percent_absent_total_to_now}%`}</Text>
                            </View>
                        </Content>
                    </Left>
                </CardItem>
            </TouchableHighlight>
        </Card>)
    }


    _onRefresh = async () => {
        await this.setState({
            refreshing: false,
            dataSource: [],
        });
        await this.getData();
        this.setState({
            refreshing: true,
        })
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    render () {
        if (!this.state.isLoading){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
                    <ActivityIndicator size="large" color="#f36523"/>
                </View>
            )
        }
        return (
            <Container>
                { this.props.navigation.getParam('isHeaderHome') && <Header hasTabs transparent />}
                <HeaderSelectDay selected={false} onValueChange={this.onValueChange} title="ĐIỂM DANH"/>
                <FlatList
                    data={this.state.dataSource.data}
                    keyExtractor={(item,index) => index.toString()}
                    onEndReached={this.loadMoreData}
                    onEndReachedThreshold ={0.3}
                    renderItem={({item}) => this.renderRow(item)}
                    ListFooterComponent={ this.renderFooter}
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            onRefresh={this._onRefresh}
                        />}

                />
                 {this.props.navigation.getParam('isHeaderHome') && <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>}
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    contentCard: {
        marginLeft: 10,
    },
    contentDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    textItem: {
        color: '#cccccc',
        fontSize: 13,
        paddingRight: 10
    },
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: 2,
        paddingRight: 5,
    },
    textTitle: {
        marginBottom: 5,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        minHeight: 60,
        fontSize: 14,
    }
});

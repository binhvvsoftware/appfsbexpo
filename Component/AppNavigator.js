import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Home from './Home';
import ScreenAP from './ScreenAP';
import HomeAP from './HomeAP';
import SlideMenuScreen from './SlideMenuScreen';
import Profile from './Profile';
import ListNewsStudy from './ListNewsStudy';
import ListNewsAction from './ListNewsAction';
import ListNewsFee from './ListNewsFee';
import ExamSchedule from './ExamSchedule';
import Transcript from './Transcript';
import HistoryLesson from './HistoryLesson';
import TuitionFee from './TuitionFee';
import Bonus from './Bonus';
import Introduction from './Introduction';
import ListContentDetail from './ListContentDetail';
import HistoryLessionItemDetail from './HistoryLessionItemDetail';
import AttendanceDetail from './AttendanceDetail';
import Attendance from './Attendance';

export const AuthStack = createStackNavigator({
    Home: {
        screen: Home,
    },
    ScreenAP: {
        screen: ScreenAP,
    },
},{ 
    headerLayoutPreset: 'center',
    initialRouteName: 'Home',
  })

export const AppNavigator = createStackNavigator({
    HomeAP: {
        screen: HomeAP,
    },
    Profile: {
        screen: Profile,
    },
    ListNewsStudy: {
        screen: ListNewsStudy,
    },
    ListNewsAction: {
        screen: ListNewsAction,
    },
    ListNewsFee: {
        screen: ListNewsFee,
    },
    ExamSchedule: {
        screen: ExamSchedule,
    },
    Transcript: {
        screen: Transcript,
    },
    HistoryLesson: {
        screen: HistoryLesson,
    },
    TuitionFee: {
        screen: TuitionFee,
    },
    Bonus: {
        screen: Bonus,
    },
    Introduction : {
        screen: Introduction,
    },
    ListContentDetail: {
        screen: ListContentDetail,
    },
    HistoryLessionItemDetail: {
        screen: HistoryLessionItemDetail,
    },
    AttendanceDetail: {
        screen: AttendanceDetail,
    },
    Attendance: {
        screen: Attendance,
    }
}, { 
  headerLayoutPreset: 'center',
});

export const mySwitchNavigator = createSwitchNavigator({
    Auth: AuthStack,
    App: AppNavigator,
})

export const MyDrawerNavigator = createDrawerNavigator({
    HomeAP: {
        screen: mySwitchNavigator,
    },
}, {
    contentComponent: props =><SlideMenuScreen {...props}/>,
});

AppNavigator.navigationOptions = ({ navigation }) => {
    let drawerLockMode = 'unlocked';
    if (navigation.state.index < 2) {
        drawerLockMode = 'locked-closed';
    }
    return {
       drawerLockMode,
    };
};



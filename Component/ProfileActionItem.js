import React from 'react';
import { ScrollView } from 'react-native';
import {  List, ListItem ,Text, Content} from 'native-base';

export default class ProfileActionItem extends React.Component {
    render () {
        return (
            <Content>
                <List>
                    <ListItem>
                        <Text note>Tổng môn chưa học : </Text>
                        <Text style={{fontWeight: 'bold'}}>0</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Tổng môn đạt : </Text>
                        <Text style={{fontWeight: 'bold'}}>100</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Tổng môn học lại : </Text>
                        <Text style={{fontWeight: 'bold'}}>0</Text>
                    </ListItem>
                    <ListItem>
                        <Text note>Tổng môn đang học : </Text>
                        <Text style={{fontWeight: 'bold'}}>0</Text>
                    </ListItem>
                </List>
            </Content>
        );
    }
}

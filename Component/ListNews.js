import React from 'react';
import { Tab, Tabs, TabHeading, Text, } from 'native-base';
import ListContent from './ListContent';

export default class ListNews extends React.Component {
    render () {
        return (
            <Tabs tabBarUnderlineStyle={{backgroundColor: 'black'}}>
                <Tab heading={ <TabHeading style={{backgroundColor: '#f36523'}}><Text style={{color: 'black'}}>Tin FSB</Text></TabHeading>}>
                    <ListContent navigation={this.props.navigation} target={1}/>
                </Tab>
                <Tab heading={ <TabHeading style={{backgroundColor: '#f36523'}}><Text style={{color: 'black'}}>Tin Đào Tạo</Text></TabHeading>}>
                    <ListContent navigation={this.props.navigation} target={2}/>
                </Tab>
            </Tabs>
        );
    }
}

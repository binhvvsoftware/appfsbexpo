import React from 'react';
import { Icon } from 'native-base';

export default myHeader = (title,navigation) => {
    const {params = {}} = navigation.state;
    return {
        title: title,
        headerTransparent: true,
        headerLeft: () => (
            <Icon ios='ios-menu' android="md-menu" onPress={() => {navigation.toggleDrawer()}}/>
        ),
        headerRight: () => (
            <Icon type="FontAwesome" name='sign-out' onPress={() => params.onLogout()}/>
        ),
        headerStyle: {
            marginHorizontal: 10,
        }
    }
 }
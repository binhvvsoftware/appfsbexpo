import React from 'react';
import { Text} from 'native-base';
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';

export default class DialogLogout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: this.props.isVisible,
        }
    }

    componentWillReceiveProps(nextProps) { 
        if (this.state.isVisible !== nextProps.isVisible){
            this.setState({
                isVisible: nextProps.isVisible,
            })
        }
    }

    render () {
        return (
            <Dialog
                visible={this.state.isVisible}
                onTouchOutside={() => {this.props.onCloseDialog()}}
                width={250}
                footer={
                    <DialogFooter>
                        <DialogButton
                            text="LOGOUT"
                            textStyle={{color: 'black', fontSize: 14, fontWeight: 'bold'}}
                            onPress={() => {this.props.handleLogout()}}
                        />
                        <DialogButton
                            text="CANCEL"
                            textStyle={{color: 'black', fontSize: 14}}
                            onPress={() => {this.props.onCloseDialog()}}
                        />
                    </DialogFooter>
                }
            >
                <DialogContent>
                    <Text style={{alignSelf: 'center', marginTop: 20}}>Bạn có muốn thoát không?</Text>
                </DialogContent>
            </Dialog>
        );
    }
}


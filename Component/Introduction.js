import React from 'react';
import { Container, Content, Header, Text} from 'native-base';
import { Image,View } from 'react-native';
import * as GoogleSignIn from 'expo-google-sign-in';
import { AsyncStorage } from 'react-native';
import myHeader from './HeaderHome';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';

export default class Introduction extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return  myHeader('Giới thiệu', navigation)
    }

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
        }
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    componentDidMount(){
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    render () {
        return (
           <Container>
                <Header hasTabs transparent />
                <View style={{flex: 1,justifyContent: 'flex-start'}}>
                    <Image style={{ width: '100%', height: 200, resizeMode: 'contain'}} source={{uri: 'http://fpt.edu.vn/Content/images/about/Fn-About_history.png'}}></Image>
                </View>
                <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>
           </Container>
        );
    }
}

import React from 'react';
import * as GoogleSignIn from 'expo-google-sign-in';
import { AsyncStorage } from 'react-native';
import { Container, Header} from 'native-base';
import ListContent from './ListContent';
import myHeader from './HeaderHome';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';

export default class ListNewsFee extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return  myHeader('Học phí', navigation)
    }

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
        }
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    componentDidMount(){
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    render () {
        return (
           <Container>
                <Header hasTabs transparent />
                <ListContent navigation={this.props.navigation} />
                <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>
           </Container>
        );
    }
}

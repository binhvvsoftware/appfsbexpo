import React from 'react';
import { Container, Content, Thumbnail, Text, Left, Body, Card, CardItem, Icon } from 'native-base';
import { StyleSheet, View, TouchableHighlight, FlatList, ActivityIndicator, RefreshControl} from 'react-native';

export default class TranscriptItem extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            page: 1,
            isLoading: false,
            refreshing: false,
        };
    }

    setModalVisible(item) {
        this.props.navigation.navigate('ListContentDetail',{ title: `Bảng điểm môn ${item.id}`, content: item});
    }
    componentDidMount(){
        this._isMounted = true;
        this.getData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getData = async () => {
        const url = `https://jsonplaceholder.typicode.com/albums?limit=30&_page=${this.state.page}`;
        fetch(url).then( response => response.json())
        .then( responseJson => {
            if (!this._isMounted){
                return;
            }
            if (responseJson.length === 0){
                this.setState({
                    isLoading: true,
                })
            }
            this.setState({
                dataSource: this.state.dataSource.concat(responseJson),
            })
        }) 
    }

    renderRow (item) {
        return (
            <Card>
                <TouchableHighlight  onPress={() => this.setModalVisible(item)}>
                    <CardItem>
                            <Left>
                                <Thumbnail square source={{ uri: 'https://znews-photo.zadn.vn/w660/Uploaded/wyhktpu/2019_06_19/5_3.jpg' }} />
                                <Content style={styles.contentCard}>
                                    <View >
                                        <Text style={styles.textTitle}>{item.id} - Nhập môn kỹ thuật phần mềm</Text>
                                    </View>
                                    <View style={styles.contentDetail}>
                                        <Text style={{color: '#2284ea'}}> Đang học</Text>
                                    </View>
                                </Content>
                            </Left>
                    </CardItem>
                </TouchableHighlight>
            </Card>
        )
    }

    loadMoreData = () => {
       this.setState({
            page: this.state.page + 1,
       }, this.getData)
    }

    _onRefresh = async () => {
        await this.setState({
            refreshing: true,
            page: 1,
            dataSource: [],
        });
        await this.getData();
        this.setState({
            refreshing: false,
        })
    }

    renderFooter = () => {
        if (this.state.isLoading){
            return <Text style={{height: 0}}></Text>;
        }
       return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',marginTop: 10, zIndex: 111, marginBottom: 20}}>
            <ActivityIndicator size="large" color="#f36523"/>
        </View>
       )
    }

    render () {
        return (
                <FlatList
                data={this.state.dataSource}
                keyExtractor={(item,index) => index.toString()}
                onEndReached={this.loadMoreData}
                onEndReachedThreshold ={0.3}
                renderItem={({item}) => this.renderRow(item)}
                ListFooterComponent={ this.renderFooter}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />}
            />
        );
    }
}

const styles = StyleSheet.create({
    contentCard: {
        marginLeft: 10,
    },
    contentDetail: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textItem: {
        color: '#cccccc',
        fontSize: 13,
    },
    iconItem: {
        fontSize: 16,
        color: '#cccccc',
        marginTop: 2,
    },
    textTitle: {
        fontWeight: 'bold',
        textTransform: 'uppercase',
        minHeight: 35,
        fontSize: 15,
    }
});
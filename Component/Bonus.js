import React from 'react';
import { Container, Content, Header } from 'native-base';
import { StyleSheet, AsyncStorage } from 'react-native';
import * as GoogleSignIn from 'expo-google-sign-in';
import BonusItem from './BonusItem';
import DialogLogout from './DialogLogout';
import { GOOGLE_LOGIN } from '../constants/configGoogleLogin';
import myHeader from './HeaderHome';

export default class Bonus extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return  myHeader('Khen thưởng/Kỷ luật', navigation)
    }

    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
        }
    }

    onCloseDialog = () => {
        this.setState({
            isVisible: false,
        })
    }

    componentDidMount(){
        this.props.navigation.setParams({
            onLogout: this.onPopupLogout.bind(this),
        })
    }

    onPopupLogout(){
        this.setState({
            isVisible: true,
        })
    }

    handleLogout = async() => {
        try {
            await this.onCloseDialog();
            await GoogleSignIn.signOutAsync();
            await AsyncStorage.setItem('user', JSON.stringify(''));
            this.props.navigation.navigate('ScreenAP');
        } catch (e) {
            console.log('cancel',e);
        }
    }

    render () {
        return (
            <Container>
                <Header hasTabs transparent />
                <BonusItem navigation={this.props.navigation}/>
                <DialogLogout navigation={this.props.navigation} isVisible={this.state.isVisible} onCloseDialog={this.onCloseDialog} handleLogout={this.handleLogout}/>
          </Container>
        );
    }
}

const styles = StyleSheet.create({

});

import React from 'react';
import { MyDrawerNavigator, AppNavigator} from './Component/AppNavigator';
import { createAppContainer } from 'react-navigation';

// const MyNavigation = createAppContainer(AppNavigator);
// const MyTabNavigator = createAppContainer(TabNavigator);
const DrawerNavigator = createAppContainer(MyDrawerNavigator);

export default function App() {
  return (
    <>
        {/* <MyNavigation/> */}
        {/* <MyTabNavigator/> */}
        <DrawerNavigator/>
    </>  
  );
};